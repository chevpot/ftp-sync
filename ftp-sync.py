from ftplib import FTP, error_perm
import os
import time
import json

config = {}


def discover_files(parent_dir, ftp=None, depth=-1):
    global config
    """ negative depths means unlimited recursion """
    files = {}

    # recursive function that collects all the ids in `acc`
    def recurse(current, depth):
        for name, facts in ftp.mlsd(current, ["type"]):
            if facts["type"] == "dir":
                recurse(f"{current}/{name}", depth)
            elif facts["type"] == "file":
                files.update(
                    {
                        name: current
                    }
                )
        return

    recurse(parent_dir, depth)
    return files


def sync_tvseries(ftp):
    global config
    files = discover_files("/tvseries", ftp=ftp)

    os.chdir(config["tvseries"])

    for filename, directory in files.items():
        if filename.endswith("partial~"):
            continue
        # Get TV Series name
        series = directory.split("/")[2]
        path = f"{config['tvseries']}/{series}"

        print(f"Download {filename} to {config['tvseries']}/{series}")

        if not os.path.exists(path):
            os.mkdir(path)

        ftp.cwd(directory)
        os.chdir(path)

        with open(f"./{filename}", "wb") as f:
            ftp.retrbinary(f"RETR {filename}", f.write)

        ftp.cwd("/")

        ftp.delete(f"{directory}/{filename}")
        print(f"Deleted {filename}")

        try:
            ftp.rmd(directory)
            print(f"Deleted {directory}")
        except error_perm as e:
            if str(e) == "550 Can't remove directory: Directory not empty":
                pass


def sync_movies(ftp):
    global config
    files = discover_files("/movies", ftp=ftp)

    os.chdir(config["movies"])

    for filename, directory in files.items():
        if filename.endswith("partial~"):
            continue
        print(f"Download {filename} to {config['movies']}")

        ftp.cwd(directory)

        with open(f"./{filename}", "wb") as f:
            ftp.retrbinary(f"RETR {filename}", f.write)

        ftp.cwd("/")

        ftp.delete(f"{directory}/{filename}")
        print(f"Deleted {filename}")

        try:
            ftp.rmd(directory)
            print(f"Deleted {directory}")
        except error_perm as e:
            if str(e) == "550 Can't remove directory: Directory not empty":
                pass


def ftpsync():
    global config

    # Load in config from
    with open("/opt/ftp-sync/config.json", "r") as f:
        config = json.load(f)

    for i in range(10):
        try:
            ftp = FTP(config["server"])
        except OSError:
            pass
        time.sleep(5)

        if i == 10:
            raise Exception("Cannot start ftplib, exiting")
    ftp.connect(port=21)
    print(f"Connected to {config['server']}:21")
    ftp.login(user=config["username"], passwd=config["password"])

    sync_movies(ftp)
    sync_tvseries(ftp)

    ftp.close()

    # Connect to ftp
    # Cascade list and cd until we find movie/tv series file
    #  while storing path along the way
    # Get movie and place in /NAS/Ingest/sabnzb
    # Delete ftp movie and parent folder(s)


if __name__ == "__main__":
    while True:
        ftpsync()
        time.sleep(60)